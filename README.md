FortyTwoRB
---

A Rubygem to demonstrate how to test, build, and publish
to [RubyGems.org](https://rubygems.org) using
using [CircleCI](https://circleci.com/). NOTE: This project references a RUBYGEMS_TOKEN. The existing token was deleted on January 6, 2023 as part of a security refresh. You'll need to create a new one!

[![CircleCI](https://circleci.com/bb/nsidc/fortytworb.svg?style=svg)](https://circleci.com/bb/nsidc/fortytworb)

[![Gem Version](https://badge.fury.io/rb/fortytworb.svg)](https://badge.fury.io/rb/fortytworb)

Prerequisites
---

* Ruby 2.3
* Bundler

Development
---

Install dependencies:

    $ bundle install

Lint and run tests:

    $ bundle exec rake rubocop
    $ bundle exec rake spec

Workflow
---

TL;DR: Use
[GitHub Flow](https://guides.github.com/introduction/flow/index.html).

In more detail:

1. Create a feature branch.
2. Create and push commits on that branch.
3. The feature branch will get built on CircleCI with each push.
4. Update the CHANGELOG with description of changes.
5. Create a Pull Request on BitBucket.
6. When the feature PR is merged, master will get built on CircleCI.

Releasing
---

1. Update the CHANGELOG to list the new version.
2. Add files and commit

        $ git add CHANGELOG.md ...
        $ git commit -m "Release v.X.Y.Z"

3. Update the version on the master branch by modifying the fortytworb
gem spec, and create a version tag in git with a leading 'v', e.g.,
'v1.0.0', 'v1.1.0', etc.

4. Push

        $ git push origin master --tags

The new version will get built
in [CircleCI](https://circleci.com/bb/nsidc/fortytworb) and pushed
to [Rubygems.org](https://rubygems.org/gems/fortytworb).

Installing
---
To install it:

    $ gem install fortytworb

License
---

Copyright 2018 National Snow and Ice Datacenter (NSIDC)
<programmers@nsidc.org>

This code is licensed and distributed under the terms of the MIT
License (see LICENSE).
