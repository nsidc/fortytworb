# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'fortytworb'
  s.version     = '2.0.0'
  s.summary     = '42'
  s.description = 'A gem to demonstrate CircleCI with rubygems.'
  s.authors     = ['Kevin Beam']
  s.email       = 'programmers@nsidc.org'
  s.files       = ['lib/fortytwo.rb']
  s.homepage    =
    'https://bitbucket.org/nsidc/fortytworb'
  s.license = 'MIT'

  s.add_development_dependency 'bundler', '~> 1.16.1'
  s.add_development_dependency 'rake', '~> 12.0.0'
  s.add_development_dependency 'rspec', '~> 3.5.0'
  s.add_development_dependency 'rspec_junit_formatter', '~> 0.2.3'
  s.add_development_dependency 'rubocop', '~> 0.48.1'
  s.add_development_dependency 'rubocop-rspec', '~> 1.15.0'
end
