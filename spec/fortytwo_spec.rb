# frozen_string_literal: true

require 'spec_helper'

describe FortyTwo do
  describe '#fortytwo' do
    it 'returns 42' do
      expect(described_class.fortytwo).to eql(42)
    end
  end
end
