# v2.0.0 (4/20/2018)

* Update README with clearer / simplified workflow.
* Switch to the MIT license.
