# frozen_string_literal: true

# returns 42
module FortyTwo
  class << self
    def fortytwo
      42
    end
  end
end
